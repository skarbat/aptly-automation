#
# Makefile
#
#  Automation of docker images creation,
#  and pkg installation tests.
#  See the README.md file for details.
#

.PHONY: all help server

# Docker aptly server image and container name
aptly-server:="aptly-server"

# Aptly API url endpoints
# More information: https://www.aptly.info/doc/api/
repo_name=sandbox
api_url=http://localhost:8080/api
api_version=$(api_url)/version
api_files=$(api_url)/files
api_repos=$(api_url)/repos
api_pkgs=$(api_url)/repos/$(repo_name)/packages

api_upload_pkg=file=@$(shell ls packages/*deb) $(api_files)/skeleton
api_publish_pkg=$(api_repos)/sandbox/file/skeleton

# notice: due to a bug in aptly 1.4.0, the publish endpoint
# needs to have 2 slashes between url and repository name,
# otherwise it will silently fail.
api_publish_repo=$(api_url)/publish//debian

# Set domain below to deploy on a public web server
NGINX_DOMAIN:="_"

# Sample package name, change to your needs
PKG:=aptly-skeleton
CLIENT:=debian

all: help

help:
	@echo "aptly-auto - Automate aptly repository testing"
	@echo
	@echo " make server-build"
	@echo " make server-up"
	@echo " make server-down"
	@echo " make server-api"
	@echo " make server-api-check"
	@echo " make server-shell"
	@echo
	@echo " make PKG=my_package_directory upgrade-pkg"
	@echo " make PKG=my_package_directory build-pkg"
	@echo " make publish-pkg"
	@echo
	@echo " make client-build"
	@echo " make CLIENT=[debian|ubuntu] client-test"
	@echo " make CLIENT=[debian|ubuntu] client-shell"
	@echo

#
# Docker server tasks
#
server-build: server-down
	cd dockers && docker build -t $(aptly-server) \
		--build-arg NGINX_SERVER_NAME=$(NGINX_DOMAIN) \
		-f Docker-aptly-server .
server-up:
	docker run -d -p 80:80 -p 8080:8080 --name=$(aptly-server) $(aptly-server)
	docker exec -u aptly $(aptly-server) /usr/local/bin/aptly-server-entrypoint baptize
server-down:
	-docker kill $(aptly-server)
	-docker rm $(aptly-server)
server-api:
	docker exec -d -u aptly $(aptly-server) aptly api serve --no-lock
server-api-check:
	curl -L $(api_version)
	@echo
	curl -L $(api_repos)
	@echo
	curl -L $(api_pkgs)
	@echo
server-shell:
	docker exec -u aptly -w /home/aptly -it $(aptly-server) bash

#
# Package tasks
#
upgrade-pkg:
	cd packages/$(PKG) && dch -m -n "upgrade test"
	make build-pkg
	make publish-pkg

build-pkg:
	-cd packages && rm -fv *
	cd packages/$(PKG) && debuild -us -uc -b

publish-pkg:
	@echo ">>> uploading package"
	curl -F $(api_upload_pkg)
	@echo "\n>>> publishing package"
	curl -X POST $(api_publish_pkg)

	@echo "\n>>> Publish update repository"
	curl -X PUT -H 'Content-Type: application/json' --data '{"ForceOverwrite": true}' $(api_publish_repo)

	@echo "\n>>> show pkgs in repo"
	curl $(api_repos)/$(repo_name)/packages
	@echo

#
# Client install tasks
#
client-build:
	cd dockers && docker build -t aptly-debian -f Docker-debian .
	cd dockers && docker build -t aptly-ubuntu -f Docker-ubuntu .
client-test:
	docker run -it --link=$(aptly-server) aptly-$(CLIENT) bash -c " \
		aptly-clients-entrypoint && \
		apt-get install -y --no-install-recommends aptly-skeleton && \
		aptly-skeleton"
client-shell:
	docker run -it --link=$(aptly-server) aptly-$(CLIENT) bash
