#!/bin/bash
#
#  aptly-admin.sh
#
#  This script provides simple steps to create and publish
#  Debian packages on a aptly repository.
#
#  Albert Casals - April 2020
#

set -x
set -e

gpg_passphrase_file=".gpg_passphrase"
sanity_file=".iam_a_sandbox"
repo_name="sandbox"
distro="debian"
components="main,testing"

function wipe_previous() {
  [ -f "$sanity_file" ] && rm -rfv .aptly*
}

function create_repo() {
  aptly repo create \
        -architectures=amd64,armhf \
        -distribution="$distro" \
        -component="$components" "$repo_name"

  aptly publish repo \
        -architectures=amd64,armhf \
        -passphrase-file="$gpg_passphrase_file" \
        -component="$components" "$repo_name" "$repo_name"

  aptly publish update \
        -passphrase-file="$gpg_passphrase_file" "$distro"

  gpg --export --armor > .aptly/public/"${repo_name}.gpg"
}

function publish_pkgs() {
  packages="$1"
  aptly repo add "$repo_name" "$packages"
  aptly publish update -passphrase-file="$gpg_passphrase_file" "$distro"
}

function repo_status() {
  aptly repo list
  aptly repo show --with-packages "$repo_name"

}

#
# Script entry point
#
command="$1"
pkgs="$2"

case "$command" in

  "create-repo")
    wipe_previous
    create_repo
    ;;

  "publish-pkgs")
    publish_pkgs "$pkgs"
    ;;

  "repo-status")
    repo_status
    ;;
  
  *)
    echo "ERROR: unrecognized command: $command"
    false
    ;;
esac

[ "$?" == "0" ] && echo "Great! Task was completed!"
