#!/bin/bash
#
#  bootstrap.sh
#
#  Execute this script on your Debian based development test system.
#  It will install all required dependencies for everythin to work.
#

sudo apt-get update
sudo apt-get install -y --no-install-recommends devscripts debhelper dh-exec build-essential curl
