## APTLY automation tests

This repository automates the deployment and testing of an aptly based Debian repository.

It is based on a Dockerized server - the aptly server - and a couple of Debian
client containers that test the installation of a sample package through apt.

The main usage is focused for Debian package developers, allowing to test
multiple deployment scenarious, before publishing it to the public.

The system also allows to deploy the aptly Docker image on a server
for public deployment of your Debian repository.

### Requirements

You will need the following:

 * A Debian-like linux on a x86_64 computer system.
 * Docker installed up and running.
 * Debian development tools to build packages
 * Have port 80 and 8080 free for allocation.

### Preparation

Execute the `bootstrap.sh` script as root to install development & packaging tools.

Run `make server-build client-build` to generate Docker images for the aptly server
and the Debian clients used for testing.

If you simply run `make` you will get a list of all the available steps. Read on...

### Environment

Running the automation package tests involves a number of steps:

 * Start the aptly server container
 * Building a sample skeleton package
 * Publishing the package to the aptly server
 * Running the Debian clients to test installation of the package

Each of these tasks can run independently through a makefile,
making the SDLC - Software Development Lifecyle, a more comfortable playground.

### Bringing up the aptly server

Assuming you are on your development computer system, we will go through
the complete installation steps.

Let us first bring up the aptly repository server:

```
$ make server-up server-api
```

At this point the aptly container, along with the REST API interface
should be up and running. Check that the repository is up and running:

```
$ make server-api-check
curl -L http://localhost:8080/api/version
{"Version":"1.4.0"}
curl -L http://localhost:8080/api/repos
[{"Name":"sandbox","Comment":"","DefaultDistribution":"debian","DefaultComponent":"main,testing"}]
curl -L http://localhost:8080/api/repos/sandbox/packages
[]
```

Visiting the url `http://localhost`, should bring you a Debian repository filespace structure,
along with the `sandbox.gpg` file used to authenticate packages.

Notice that the `/pool` directory is empty. This is because we have not published any packages yet.
We are now ready to generate a debian package and push it to this repository.

### Publishing a sample package

The directory `packages` contains a sample skeleton that you can use to play with. Let us build it.

```
$ make build-pkg
```

The package is available at the `packages` directory. Let us now push it to the repository.

```
$ make publish-pkg
>>> uploading package
curl -F file=@packages/aptly-skeleton_2.1.0_all.deb http://localhost:8080/api/files/skeleton
["skeleton/aptly-skeleton_2.1.0_all.deb"]
>>> publishing package
curl -X POST http://localhost:8080/api/repos/sandbox/file/skeleton
{"FailedFiles":[],"Report":{"Warnings":[],"Added":["aptly-skeleton_2.1.0_all added"],"Removed":[]}}
>>> Publish update repository
curl -X PUT -H 'Content-Type: application/json' --data '{"ForceOverwrite": true}' http://localhost:8080/api/publish//debian
{"AcquireByHash":false,"Architectures":["amd64","armhf"],"ButAutomaticUpgrades":"","Distribution":"debian","Label":"","NotAutomatic":"","Origin":"","Prefix":".","SkipContents":false,"SourceKind":"local","Sources":[{"Component":"testing","Name":"sandbox"},{"Component":"main","Name":"sandbox"}],"Storage":""}
>>> show pkgs in repo
curl http://localhost:8080/api/repos/sandbox/packages
["Pall aptly-skeleton 2.1.0 b5834c6b24783d27"]
```

The make step has contacted the aptly server through the REST API to upload the package,
and then publish it on the repository.

Open your web browser and visit `http://localhost/pool`, you will notice
`main` and `testing`. The sampe package will have been published at both components.

You can also visit the API interface, which has a cool option to display a map
of the repository and available packages, try: `http://localhost:8080/api/graph.png?layout=vertical`

`make server-api-check` will display the new package availability.

### Testing the package

It is now time to use the Debian clients to test the package installation.
Note that in order to test the client images you need to have the server up and running.

```
make client-test
```

This step will run the Debian container, add the apt sources from the APTLY server,
and install the package we generated earlier. By default it will user Debian Buster.

In order to run the same test on Ubuntu 19.10, do:

```
$  make CLIENT=ubuntu client-test
```

### Upgrading a package

During your SDLC process, you will need to test package upgrades. Here is how to run them.

First, open an interactive shell to the Debian container: `make client-shell`,
and register the APTLY repository by executing `aptly-clients-entrypoint`.

Calling `apt-cache show aptly-skeleton` should bring you details about our latest published package.
Open a new terminal to generate and publish a new package:

```
$ make upgrade-pkg
```

This will increase the package changelog, rebuild the package and publish it into the APTLY container.
Stepping back to the Debian shell...

```
$ apt-get update
$ apt-get install aptly-skeleton
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following packages will be upgraded:
  aptly-skeleton
1 upgraded, 0 newly installed, 0 to remove and 1 not upgraded.
Need to get 1604 B of archives.
After this operation, 0 B of additional disk space will be used.
Get:1 http://aptly-server debian/main amd64 aptly-skeleton all 2.1.0+nmu7 [1604 B]
Fetched 1604 B in 0s (0 B/s)             
(Reading database ... 8596 files and directories currently installed.)
Preparing to unpack .../aptly-skeleton_2.1.0+nmu7_all.deb ...
Unpacking aptly-skeleton (2.1.0+nmu7) over (2.1.0+nmu6) ...
Setting up aptly-skeleton (2.1.0+nmu7) ...
root@ff3cd3ae6d8c:/# dpkg -l aptly-package
dpkg-query: no packages found matching aptly-package
root@ff3cd3ae6d8c:/# dpkg -l aptly-skeleton
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
|/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
||/ Name           Version      Architecture Description
+++-==============-============-============-===============================================================
ii  aptly-skeleton 2.1.0+nmu7   all          A sample skeleton package to automate aptly repository testing.
```

If you need to build your own package, simply copy it into the `packages` directory, and do this:

```
$ make PKG=mypkg uprade-pkg
```

### Dockerhub

The aptly server Docker image is available at Docker Hub - see references section below.
If you want to deploy the image to be public audience, consider the following:

 * Do not run the REST API inside the image, it imposes a number of risks
 * Use Docker volumes to store the repository database and GPG keys outside the container
 * Consider the need to have ssh access to the container for aptly maintenance tasks.

#### References

 * https://www.aptly.info/
 * https://www.aptly.info/doc/api/
 * https://github.com/mikepurvis/aptly-docker
 * https://hub.docker.com/repository/docker/skarbat/aptly-server

(c) Albert Casals - skarbat@gmail.com - April 2020
